package ru.pisarev.tm.api;

import ru.pisarev.tm.model.Project;

import java.util.List;

public interface IProjectService {
    List<Project> findAll();

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(Integer index);

    void add(Project project);

    void remove(Project project);

    Project removeById(String id);

    Project removeByName(String Name);

    Project removeByIndex (Integer index);

    void clear();

    Project updateById(final String id, final String name, final String description);

    Project updateByIndex(final Integer index, final String name, final String description);
}
